<?php
namespace Esc\Shopify\Providers;

use Illuminate\Support\ServiceProvider;

use Esc\Shopify\API;


class APIServiceProvider extends ServiceProvider {
    public function register() {
        // Publish configuration
        $this->publishes([
            __DIR__.'/../config/esc_shopify.php' => config_path('esc_shopify.php')
        ]);

        $this->app->bind('ShopifyAPI', function($app) {
            return new API;
        });

        $this->app->bind(Esc\Shopify\API::class, function($app) {
            return new API;
        });

        $this->app->bind('App\Shop', function($app) {
            if (\Auth::user()) {
                return \Auth::user()->shop;
            }
        });
    }

    public function boot() {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/../routes.php';
        }
    }
}
